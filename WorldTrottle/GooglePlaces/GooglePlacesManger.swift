//
//  GooglePlacesManger.swift
//  WorldTrottle
//
//  Created by Abdo Emad on 05/05/2024.
//

import GooglePlaces

struct GooglePlace {
    let name: String
    let details: String?
    let id: String
    let distance: Double?
}

final class GooglePlacesManger {
    
    static let shared = GooglePlacesManger()
    private let client = GMSPlacesClient.shared()
    
    private init(){}
    
    public func findPlaces(query: String, completion: @escaping (Result<[GooglePlace],Error>) -> Void ) {
        let filter = GMSAutocompleteFilter()
        client.findAutocompletePredictions(
            fromQuery: query,
            filter: filter,
            sessionToken: nil) { [weak self] results, error in
                guard let _ = self else {return}
                if let error = error {
                    completion(.failure(error))
                    return
                }
                guard let results = results else {
                    return
                }
                let places: [GooglePlace] = results.compactMap { place in
                    GooglePlace(name: place.attributedPrimaryText.string, details: place.attributedSecondaryText?.string, id: place.placeID, distance: Double(truncating: place.distanceMeters ?? 0))
                }
                completion(.success(places))
                return
            }
    }
    
    public func get(place x: GooglePlace, completion: @escaping (Result<Location,Error>) -> Void ) {
        
        client.fetchPlace(
            fromPlaceID: x.id,
            placeFields: .coordinate,
            sessionToken: nil) { [weak self] (place, error) in
                guard let _ = self else {return}
                if let error = error {
                    completion(.failure(error))
                    return
                }
                guard let place = place else {
                    return
                }
                completion(
                    .success(
                        Location(
                            latitude: place.coordinate.latitude,
                            longitude: place.coordinate.longitude,
                            address: x.name
                        )
                    )
                )
            }
    }
    
}


