//
//  MapViewController.swift
//  WorldTrottle
//
//  Created by abdo emad  on 28/01/2024.
//

import UIKit
import GoogleMaps
import CoreLocation


class MapViewController: UIViewController {
    
    
    @IBOutlet weak var centerMarkerImageView: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var loadingLable: UILabel!
    @IBOutlet weak var addressContinerView: UIVisualEffectView!
    
    //MARK: - Properties
    lazy var mapView : GMSMapView = GMSMapView()
    var location: Location?
    var locationManager = CLLocationManager ()
    private lazy var search = UISearchController(searchResultsController: SearchResultVC(delegate: self))
    
    //MARK: - Initiation
    init(loaction: Location){
        self.location = loaction
        super.init(nibName: "MapViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    //MARK: - life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSegmentdControl()
        setupDesing()
        setupMapView()
        setupNavigationDesign()
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
    }
    
    //MARK: - set up
    
    func setupDesing(){
        addressContinerView.layer.cornerRadius = 16
        addressContinerView.clipsToBounds = true
        loadingLable.text = "Loading Loaction"
    }
    
    private func setupNavigationDesign() {
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController = self.search
        self.search.searchResultsUpdater = self
    }
    func startLoading(){
        addressLabel.isHidden = true
        loadingLable.isHidden = false
    }
    
    func stopLoading(){
        addressLabel.isHidden = false
        loadingLable.isHidden = true
    }
    
    func addSegmentdControl(){
        
        if let path = Bundle.main.path(forResource: "Localizable.strings", ofType: "txt") {
            do {
                let content = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
                print(content)
            } catch {
                print("Error reading the file: \(error)")
            }
        }
        let standardString = NSLocalizedString("Standard", comment: "Standard Map View")
        let hybridString = NSLocalizedString("Hybrid", comment: "Hybrid Map View")
        let segmentedControl = UISegmentedControl(items: [standardString, hybridString])
        
        segmentedControl.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        segmentedControl.selectedSegmentIndex = 0
        segmentedControl.selectedSegmentTintColor = UIColor.tintColor
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(segmentedControl)
        
        let margins = view.layoutMarginsGuide
        
        segmentedControl.topAnchor.constraint(equalTo: view.topAnchor ,constant: 150).isActive = true
        segmentedControl.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        segmentedControl.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
        
        
        segmentedControl.addTarget(self, action: #selector(self.mapTypeChanged(_:)), for: .valueChanged)
    }
    
    @objc func mapTypeChanged(_ segController : UISegmentedControl){
        
        switch segController.selectedSegmentIndex {
        case 0:
            mapView.mapType = .normal
        case 1:
            mapView.mapType = .hybrid
        default:
            break
        }
    }
    
}

//MARK: -  MapVeiw

extension MapViewController{
    func setupMapView(){
        mapView.backgroundColor = .systemBackground
        mapView.delegate = self
        view.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        view.sendSubviewToBack(mapView)
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
        
    }
    
}
extension MapViewController: GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        startLoading()
        location = nil
        
        let latitude = position.target.latitude
        let longitude = position.target.longitude
        
        getAddressFrom(latitude: latitude, longitude: longitude) { address in
            if let address = address{
                self.addressLabel.text = address
                self.location = .init(
                    latitude: latitude,
                    longitude: longitude,
                    address: address)
                self.stopLoading()
            }
            self.centerMarkerImageView.image = UIImage(resource: .markerSelectLocation)
            
        }
        
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.centerMarkerImageView.image = UIImage(resource: .markerSelectingLocation)
    }
    
}

//MARK: - Get Current Loocation

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {return}
        
        let latitude = location.coordinate.latitude
        let longitude = location.coordinate.longitude
        
        getAddressFrom(latitude: latitude, longitude: longitude) { address in
            if let address = address{
                self.addressLabel.text = address
                self.location = .init(
                    latitude: latitude,
                    longitude: longitude,
                    address: address)
                self.stopLoading()
                
                if let location = self.location {
                    self.mapView.animate(toLocation: location.coordinate)
                    self.mapView.animate(toZoom: 16)
                }
            }
            self.locationManager.stopUpdatingLocation()
            self.centerMarkerImageView.image = UIImage(resource: .markerSelectLocation)
            
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location manager failed with error: \(error.localizedDescription)")
    }
}


//MARK: - get address

extension MapViewController{
    
    func getAddressFrom(latitude: Double, longitude: Double, completion: @escaping (_ address: String?) -> Void) {
        let geocoder = CLGeocoder()
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        center.latitude = latitude
        center.longitude = longitude
        let location: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        geocoder.reverseGeocodeLocation(location) { (placeMarks, error) in
            if (error != nil) {
                print("reverse geocoder fail: \(error!.localizedDescription)")
                completion(nil)
                return
            }
            guard let places = placeMarks, let place = places.first else {
                completion(nil)
                return
            }
            
            let addressStrings = [
                place.thoroughfare,
                place.locality,
                place.country
            ]
            
            completion(addressStrings.compactMap({$0}).joined(separator: ","))
        }
    }
    
}




//MARK: - UISearchResultsUpdating -


extension MapViewController : UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        guard let query = searchController.searchBar.text,
              !query.trimWhiteSpace().isEmpty,
              let resultVC = searchController.searchResultsController as? SearchResultVC
        else {return}
        GooglePlacesManger.shared.findPlaces(query: query) { [weak self ] result in
            guard let _ = self else {return}
            switch result {
            case .success(let success):
                resultVC.update(with: success)
            case .failure:
                return
            }
        }
    }
    
}

//MARK: - SearchResultDelegate -

extension MapViewController: SearchResultDelegate{
    func didSelect(place: GooglePlace) {
        startLoading()
        search.dismiss(animated: true)
        GooglePlacesManger.shared.get(place: place) { [weak self] result in
            guard let self = self else {return}
            self.stopLoading()
            switch result {
            case .success(let success):
                self.mapView.animate(toLocation: .init(latitude: success.latitude, longitude: success.longitude))
            case .failure(let failure):
                printContent(failure.localizedDescription)
            }
        }
    }
    
    
}


extension String {
    func trimWhiteSpace() -> String{
        let newValue = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return newValue
    }
}
