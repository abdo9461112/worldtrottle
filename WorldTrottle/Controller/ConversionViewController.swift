//
//  ViewController.swift
//  WorldTrottle
//
//  Created by abdo emad  on 25/01/2024.
//

import UIKit

class ConversionViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var celsiuslable: UILabel!
    
    
    var fahrenheitValue: Measurement<UnitTemperature>?{
        didSet{
            updateCelsiusLable()
        }
    }
    var celsiusValue: Measurement<UnitTemperature>? {
        if let fahrenheitValue = fahrenheitValue {
            return fahrenheitValue.converted(to: .celsius)
        } else {
            return nil
        }
    }
    
    
    let numberFormatter : NumberFormatter = {
        let nF = NumberFormatter()
        nF.numberStyle = .decimal
        nF.minimumFractionDigits = 0
        nF.maximumFractionDigits = 1
        
        let currentLocale = Locale.current
        let isMetric = currentLocale.identifier
        let currencySymbol = currentLocale.currencySymbol

        return nF
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        valueTextField.delegate = self
        updateCelsiusLable()
        
    }
    

    
    func updateCelsiusLable(){
        if let celsiusValue = celsiusValue{
            celsiuslable.text = numberFormatter.string(from: NSNumber(value: celsiusValue.value))
        }else{
            celsiuslable.text = "????"
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        valueTextField.resignFirstResponder()
        
    }
    
    
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        if let text = textField.text, let number = numberFormatter.number(from: text) { fahrenheitValue = Measurement(value: number.doubleValue, unit: .fahrenheit)

        } else {
            celsiuslable.text = "????"
            
        }
        
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool {
        
        let forbiddenCharacters = CharacterSet(charactersIn: "QWERTYUIOPLKJHGFDSAZXCVBNMqwertyuioplkjhgfdsazxcvbnm!@#$%^&*()_+{}[]|\"':;<>,?/~`ضصثقفغعهخحجكمنتالبيسشـظطذدزرو،/")
        
        let currentLocale = Locale.current
        let decimalSeparator = currentLocale.decimalSeparator ?? "."
        
        
        let existingTextHasDecimalSeparator = textField.text?.range(of: decimalSeparator)
        let replacementTextHasDecimalSeparator = string.range(of: decimalSeparator)
            
        if let _ = string.rangeOfCharacter(from: forbiddenCharacters){
            return false
        }
        if existingTextHasDecimalSeparator != nil, replacementTextHasDecimalSeparator != nil {
            return false
        } else {
            return true
            
        }
        
    }
    
}


    
