//
//  SearchResultVC.swift
//  WorldTrottle
//
//  Created by Abdo Emad on 05/05/2024.
//
protocol SearchResultDelegate: AnyObject {
    func didSelect(place: GooglePlace)
}

import UIKit

class SearchResultVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var places : [GooglePlace] = []
    weak var delegate : SearchResultDelegate?
    
    init(delegate : SearchResultDelegate?){
        self.delegate = delegate
        super.init(nibName: "SearchResultVC", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "SearchLocationTableViewCell", bundle: nil),forCellReuseIdentifier: "cell")
    }

    func update(with data: [GooglePlace]) {
        self.places = data
        self.tableView.reloadData()
    }

}

//MARK: - tableView



extension SearchResultVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let place = places[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SearchLocationTableViewCell
        cell.set(
            title: place.name,
            detail: place.details)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let place = places[indexPath.row]
        delegate?.didSelect(place: place)
    }
    
    
}
