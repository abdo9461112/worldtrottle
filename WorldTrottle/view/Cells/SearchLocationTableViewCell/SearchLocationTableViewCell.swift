//
//  SearchLocationTableViewCell.swift
//  WorldTrottle
//
//  Created by Abdo Emad on 05/05/2024.
//

import UIKit

class SearchLocationTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var detailLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupDesing()
    }

    //MARK: - Desing
    
    func setupDesing(){
        selectionStyle = .none
    }
    
    func set(title : String?, detail: String?){
        titleLable.text = title
        detailLable.text = detail
    }

}
