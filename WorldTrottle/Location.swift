//
//  Location.swift
//  WorldTrottle
//
//  Created by Abdo Emad on 05/05/2024.
//

import CoreLocation

struct Location{
    let latitude: Double
    let longitude: Double
    var address: String
    
    var coordinate : CLLocationCoordinate2D {
        .init(latitude: latitude, longitude: longitude)
    }
}
